/**
 * Copyright 2021 Erick Veil
 */

#ifndef CLIENTRUNNER_H
#define CLIENTRUNNER_H

#include "universalsocketclient.h"
#include <QString>
#include <functional>
#include <QByteArray>
#include <QTimer>
#include "staticlogger.h"


class clientRunner : public QObject
{
    Q_OBJECT

    int msgId = 1;
    evtools::UniversalSocketClient client;

public:
    clientRunner();

    void start();
    void successCb();

private:
    QByteArray buildMsg();
    void _clientSend(QByteArray msg);

public slots:
    void timerCb();

};

#endif // CLIENTRUNNER_H
