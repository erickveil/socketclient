/** Copyright 2018 Erick Veil */

#include "universalsocketclient.h"

namespace evtools {

UniversalSocketClient::UniversalSocketClient()
{
    _isDataInitialised = false;
    _isSlotsConnected = false;
}

UniversalSocketClient::UniversalSocketClient(QString ip, int port)
{
    setConnectData(ip, port);
    initSignalsAndSlots();
}

UniversalSocketClient::UniversalSocketClient(
        QString ip, int port, std::function<void (QByteArray)> successCallback)
{
    setConnectData(ip, port);
    initSignalsAndSlots();
    setSuccessCallback(successCallback);
}

UniversalSocketClient::UniversalSocketClient(
        QString ip, int port, std::function<void (QByteArray)> successCallback,
        std::function<void (
            QString, QAbstractSocket::SocketError)> errorCallback)
{
    setConnectData(ip, port);
    initSignalsAndSlots();
    setSuccessCallback(successCallback);
    setErrorCallback(errorCallback);
}

UniversalSocketClient::~UniversalSocketClient()
{
    QString logMsg;
    if (!isClientDisconnected()) {
        logMsg = "Attempting to destroy connected client.";
        LOG_ERROR(logMsg);
        _socket.disconnectFromHost();
        _socket.waitForDisconnected();
    }

    _isDoneOnce = true;
    LOG_DEBUG(getName() + " is destroyed.");
}

bool UniversalSocketClient::isNull()
{
    return !(_isDataInitialised && _isSlotsConnected);
}

bool UniversalSocketClient::isDoneOnce()
{
    return _isDoneOnce && isClientDisconnected();
}

QString UniversalSocketClient::getName()
{
    return "Client@" + _socketIp + ":" + QString::number(_socketPort) +
            "(uuid: " + _uniqueId.toString() + ")";
}

QString UniversalSocketClient::getIp()
{
    return _socketIp;
}

int UniversalSocketClient::getPort()
{
    return _socketPort;
}

void UniversalSocketClient::_sendData()
{
    LOG_DEBUG("Sending...");

    qint64 bytesWritten = _socket.write(_nextMsg, _nextMsg.length());
    Q_UNUSED(bytesWritten);

    LOG_DEBUG(QString::number(bytesWritten) + " bytes written");

    // "Should not need," but sometimes do...
    bool isDataFlushed = _socket.flush();
    if (isDataFlushed) {
        LOG_DEBUG(" Data flushed.");
    }
    else {
        LOG_DEBUG(" No data flushed.");
    }
}

void UniversalSocketClient::setConnectData(QString ip, int port)
{
    _socketIp = ip;
    _socketPort = port;
    _isDataInitialised = true;
    bool socketDisabled = port <= 0;
    if (socketDisabled) {
        QString msg = getName() + " socket is disabled with port " +
                QString::number(port);
    }
    // experimental:
    //_socket.setSocketOption(QAbstractSocket::SocketOption::LowDelayOption, 1);
}

void UniversalSocketClient::initSignalsAndSlots()
{
    if (_isSlotsConnected) { return; }

    // Connect QAbstract Socket Stuff
    connect(&_socket, SIGNAL(connected()),
            this, SLOT(_eventSocketConnected()));
    connect(&_socket, SIGNAL(disconnected()),
            this, SLOT(_eventSocketDisconnected()));
    connect( &_socket,
             QOverload<QAbstractSocket::SocketError>::
                of(&QAbstractSocket::error),
             [=](QAbstractSocket::SocketError socketError) {
        _eventSocketError(socketError);
    });
    connect(&_socket, SIGNAL(hostFound()),
            this, SLOT(_eventSocketHostFound()));
    connect(&_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)),
            this, SLOT(_eventSocketStateChanged(QAbstractSocket::SocketState)));

    // Connect QIODevice Stuff
    connect(&_socket, SIGNAL(aboutToClose()),
            this, SLOT(_eventIODeviceAboutToClose()));
    connect(&_socket, SIGNAL(bytesWritten(qint64)),
            this, SLOT(_eventIODeviceBytesWritten(qint64)));
    connect(&_socket, SIGNAL(channelBytesWritten(int,qint64)),
            this, SLOT(_eventIODeviceChannelBytesWritten(int, qint64)));
    connect(&_socket, SIGNAL(channelReadyRead(int)),
            this, SLOT(_eventIODeviceChannelReadyRead(int)));
    connect(&_socket, SIGNAL(readChannelFinished()),
            this, SLOT(_eventIODeviceReadFinished()));
    connect(&_socket, SIGNAL(readyRead()),
            this, SLOT(_eventIODeviceReadyRead()));
    connect(&_readTimer, SIGNAL(timeout()),
            this, SLOT(_eventReadTimeout()));
    _readTimer.setSingleShot(true);

    _isSlotsConnected = true;
    _uniqueId = QUuid::createUuid();
}

void UniversalSocketClient::setSuccessCallback(
        std::function<void (QByteArray)> successCallback)
{
    _successCallback = successCallback;
}

void UniversalSocketClient::setSuccessCallback(
        std::function<void (QByteArray, QByteArray)> successCallback)
{
    _successCbWithSent = successCallback;
}

void UniversalSocketClient::setErrorCallback(
        std::function<void (
            QString, QAbstractSocket::SocketError)> errorCallback)
{
    _errorCallback = errorCallback;
}

void UniversalSocketClient::setErrorCallback(
        std::function<void (QByteArray, QString,
                            QAbstractSocket::SocketError)> errorCallback)
{
    _errorCbWithSent = errorCallback;
}

void UniversalSocketClient::setConnectionCallback(
        std::function<void ()> connectCallback)
{
    _connectionCallback = connectCallback;
}

void UniversalSocketClient::setIsConstantConnection(bool isConstConnect)
{
    _isConstantConnection = isConstConnect;
}

void UniversalSocketClient::setReadTimeout(int timeMs)
{
    _readTimeoutMs = timeMs;
}

void UniversalSocketClient::setIsTimeoutRead(bool isTimeoutRead)
{
    _isTimeOutRead = isTimeoutRead;
}

void UniversalSocketClient::setIsExpectingResponse(bool isExpectingResponse)
{
    _isExpectingResponse = isExpectingResponse;
}

void UniversalSocketClient::sendToSocket(QByteArray msg)
{
    LOG_DEBUG(getName() + " Send to socket called");

    _isLastHostFound = false;
    _lastBytesWritten = 0;

    // Skip quietly if disabled
    bool isDisabled = _socketPort <= 0;
    if (isDisabled) { return; }

    // Find what's trying to send an empty payload and fix it.
    if (msg.isEmpty()) {
        LOG_WARN("Empty payload send attempt.");
        return;
    }

    _readTimer.stop();

    if (isNull()) {
        QString logMsg = "Attempting to send on an uninitialized socket.";
        LOG_ERROR(logMsg);
        return;
    }

    if (!isClientDisconnected()) {
        // if this spams the log, it needs to be addressed and fixed, not
        // commented out.
        QString msg = getName() + "Socket is being activated while already "
                                  "in use. Closing.";
        LOG_WARN(msg);
        _socket.disconnectFromHost();
        _isDoneOnce = true;
        _readTimer.stop();
        if (_socket.isOpen()) { _socket.abort(); }
    }

    // Just initializing the value. Gets set to true once we read a response.
    _isResponded = false;

    _nextMsg = msg;
    QHostAddress hostAddr(_socketIp);
    _socket.connectToHost(hostAddr, _socketPort);
    // Signals handle the rest.
}

void UniversalSocketClient::startConstantConnection()
{

    // Skip quietly if disabled
    bool isDisabled = _socketPort <= 0;
    if (isDisabled) { return; }

    LOG_DEBUG(getName() + " Starting constant connection");
    _isLastHostFound = false;
    _lastBytesWritten = 0;
     if (isNull()) {
        QString logMsg = "Attempting to send on an uninitialized socket.";
        LOG_WARN(logMsg);
        return;
    }

    _nextMsg = "";
    QHostAddress hostAddr(_socketIp);
    _socket.connectToHost(hostAddr, _socketPort);
    // Signals handle the rest.
}

void UniversalSocketClient::sendDataToConstantConnection(QByteArray msg)
{
    if (!_isConstantConnection) {
        LOG_ERROR(getName() +
                    ": Attempting to send data to an unconnected socket. "
                  "Make sure this client is a constant connection client"
                  "before useing this method.");
        return;
    }

    if (!_socket.isOpen()) {
        LOG_DEBUG(getName() +
                  ": Attempting to write to a closed socket.");
        return;
    }

    _nextMsg = msg;
    _sendData();
}

bool UniversalSocketClient::isClientDisconnected()
{
    //QAbstractSocket::SocketState state = _socket.state();
    //return _socket.state() != QAbstractSocket::ConnectedState;
    return _socket.state() == QAbstractSocket::UnconnectedState;
}

void UniversalSocketClient::_eventSocketConnected()
{
    LOG_DEBUG(getName() + " Socket Connected");

    //qDebug() << "Socket Connected.";
    _isLastConnectionSuccessful = true;
    if (_connectionCallback) { _connectionCallback(); }

    if (_isConstantConnection) { return; }

    _sendData();
}

void UniversalSocketClient::_eventSocketDisconnected()
{
    //qDebug() << "Socket Disconnected.";
    LOG_DEBUG(getName() + " disconnected.");
    _isDoneOnce = true;

}

void UniversalSocketClient::_eventSocketError(QAbstractSocket::SocketError err)
{
    QString logMsg = getName() + ": Socket error event fired: " +
            _socket.errorString();
    LOG_DEBUG(logMsg);

    switch(err) {
    case QAbstractSocket::RemoteHostClosedError:
        if (!_isConstantConnection) { return; }
        if (_errorCallback) {
            _isLastConnectionSuccessful = false;
            _errorCallback(_socket.errorString(), err);
        }
        if (_errorCbWithSent) {
            _isLastConnectionSuccessful = false;
            _errorCbWithSent(_nextMsg, _socket.errorString(), err);
        }
        if (!_errorCallback && !_errorCbWithSent) {
            LOG_WARN(getName() +
                     ": Socket RemoteHostClosedError without callback: " +
                     _socket.errorString());
        }
        break;
    default:
        //qDebug() << "Socket Error.";
        //qDebug() << _socket.errorString();
        if (_errorCallback) {
            _isLastConnectionSuccessful = false;
            _errorCallback(_socket.errorString(), err);
        }
        if (_errorCbWithSent) {
            _isLastConnectionSuccessful = false;
            _errorCbWithSent(_nextMsg, _socket.errorString(), err);
        }
        if (!_errorCallback && !_errorCbWithSent) {
            LOG_WARN(getName() + ": Socket Error without callback: " +
                     _socket.errorString());
        }
    }
    if (_socket.isOpen()) {
        _socket.disconnectFromHost();
        _isDoneOnce = true;
        _readTimer.stop();
    }
}

void UniversalSocketClient::_eventSocketHostFound()
{
    /* NOTE: This event is fired on a client **even if there is no such host
     * on the network!**
     * In a failure to connect for hub status requests, the flag still comes up
     * false, because the Send method for the next poll is activated before the
     * socket is hung up. So we have
     *
     * - this attempt started,
     * - the flag set to true
     * - the next attempt started,
     * - the host flag set to false
     * - the last connection attempt closes
     * - the closing error checks report the false state of the flag.
     *
     * It's wonky, but it works. However, you might see the host found on
     * connections that fail which are not so regular that they interrupt each
     * other.
     */
    LOG_DEBUG(getName() + " Host found");
    //qDebug() << "Socket Host Found.";
    _isLastHostFound = true;

}

void UniversalSocketClient::_eventSocketStateChanged(
        QAbstractSocket::SocketState state)
{
    switch (state) {
    case QAbstractSocket::UnconnectedState:
        LOG_DEBUG(getName() + " State changed: unconnected");
        break;
    case QAbstractSocket::HostLookupState:
        LOG_DEBUG(getName() + " State changed: host lookup");
        break;
    case QAbstractSocket::ConnectingState:
        LOG_DEBUG(getName() + " State changed: connecting");
        break;
    case QAbstractSocket::ConnectedState:
        LOG_DEBUG(getName() + " State changed: connectED");
        break;
    case QAbstractSocket::BoundState:
        LOG_DEBUG(getName() + " State changed: bound");
        break;
    case QAbstractSocket::ClosingState:
        LOG_DEBUG(getName() + " State changed: closing");
        break;
    case QAbstractSocket::ListeningState:
        LOG_DEBUG(getName() + " State changed: listening");
        break;
    default:
        LOG_DEBUG(getName() + " State changed: unknown");
        break;
    }

}

void UniversalSocketClient::_eventIODeviceAboutToClose()
{
    LOG_DEBUG(getName() + " IO device about to close");

}

void UniversalSocketClient::_eventIODeviceBytesWritten(qint64 bytes)
{
    LOG_DEBUG(getName() + " IO device bytes written: " + QString::number(bytes) + " bytes");
    _lastBytesWritten = bytes;

    /* Once we've successfully written to the socket, we may begin awaiting
     * a response.
     * This forces an earlier timeout when we are waiting for the response to
     * come. We really shouldn't wait more than a second.
     * If it's a constant connection, we don't want to time out, though.
    */
    _socket.flush();
    if (_isConstantConnection) {return;}
    _readTimer.start(_readTimeoutMs);
}

void UniversalSocketClient::_eventIODeviceChannelBytesWritten(int channel,
                                                              qint64 bytes)
{
    LOG_DEBUG(getName() + " IO device channel bytes written: Channel " + QString::number(channel) + ", " + QString::number(bytes) + " bytes");
    Q_UNUSED(channel);
    Q_UNUSED(bytes);

    //qDebug() << "IO Device Channel Bytes Written.";
}

void UniversalSocketClient::_eventIODeviceChannelReadyRead(int channel)
{
    LOG_DEBUG(getName() + " IO device channel ready read");
    Q_UNUSED(channel);

    /* We reach this point only when the listener has provided a response.
     * So we may mark the flag as responded so that we bypass an error
     * condition when the read timer expires.
     */
    _isResponded = true;

    QByteArray response;
    response.clear();

    while (_socket.bytesAvailable() > 0) {
        if (!NumericAckControl) {
            response.append(_socket.readAll());
            _socket.flush();
            continue;
        }

        // numeric ack control
        QByteArray line = _socket.readLine();
        LOG_INFO("Bytes read: " + QString::number(line.length()) +
                 ", Bytes available to read: " +
                 QString::number(_socket.bytesAvailable()) +
                 " Ack string: " + line);
        response.append(line);

        bool ok = true;
        int ackVal = line.trimmed().toInt(&ok);
        if (!ok) {
            LOG_WARN("Expected an int string as an ack: " + line);
            continue;
        }
        bool isAllBytesReceived = (ackVal == _nextMsg.count());
        if (!isAllBytesReceived) { continue; }
        if (_isConstantConnection) { continue; }
        LOG_INFO("Full message acknowledged. Closing connection.");
        _socket.disconnectFromHost();
        _isDoneOnce = true;
        _readTimer.stop();
        return;
    }

    LOG_DEBUG(getName() + " Total Ack Bytes read: " +
             QString::number(response.length()));
    if (!NumericAckControl) { LOG_DEBUG("Ack from server: " + response); }
    else { LOG_DEBUG("Non numeric ack control"); }

    _lastResponse = response;
    if (!_isConstantConnection) {
        if (!NumericAckControl) { _socket.disconnectFromHost(); }
        _isDoneOnce = true;
        _readTimer.stop();
    }
    else {
        /* During a constant connection, there is no hangup, so this doesn't get
         * called naturally and we need to explicitly end the single communication
         * here.
         */
        emit _socket.readChannelFinished();
    }

}

void UniversalSocketClient::_eventIODeviceReadFinished()
{
    LOG_DEBUG(getName() + " IO device read finished");
    LOG_DEBUG("Sent: " + QString::number(_nextMsg.count()) + " bytes");

    if (_lastBytesWritten == 0) {
        if (_isConstantConnection) {
            LOG_INFO(getName() + ": Constant connection disconnected without "
                                 "writing any data.");
        }
        else {
            LOG_WARN(getName() +
                 ": Could not write to destination (Possibly cannot reach "
                 "destionation)");
        }
    }
    if (!_isLastHostFound) {
        LOG_WARN(getName() + ": Could not find host on network.");
    }
    if (!_isLastConnectionSuccessful) {
        QString errmsg;
        if (_isConstantConnection) {
            errmsg = getName() + ": Constant connection attempt not successful "
                                 "or disconnected.";
        }
        else {
            errmsg = getName() + ": Connection attempt was not successful.";
        }
        LOG_WARN(errmsg);
        // Let fall through to try to parse, just in case. (Maybe my flag setting
        // is in a bad race condition?)
    }

    if (_socket.isOpen() && !_isConstantConnection) {
        _socket.disconnectFromHost();
        _isDoneOnce = true;
        _readTimer.stop();
    }
    if (!_isLastConnectionSuccessful) { return; }
    if (_successCallback) { _successCallback(_lastResponse); }
    if (_successCbWithSent) { _successCbWithSent(_nextMsg, _lastResponse); }
}

void UniversalSocketClient::_eventIODeviceReadyRead()
{
    LOG_DEBUG(getName() + " IO device ready read");
    // The read routine was moved to IODeviceChannelReadyRead, because it
    // comes second.
    // This keeps the debugging output in order
}

void UniversalSocketClient::_eventReadTimeout()
{
    if (!_isTimeOutRead) { return; }

    LOG_INFO(getName() + " read timeout");
    if (_socket.isOpen() && !_isConstantConnection) {
        _socket.disconnectFromHost();
        _isDoneOnce = true;
        _readTimer.stop();
    }

    if (_isResponded) {  return;  }

    // No response.
    _lastResponse.clear();

    // No response is considered normal by this client if the flag is set.
    if (!_isExpectingResponse) {
        _isLastConnectionSuccessful = true;
        if (_successCallback) { _successCallback(_lastResponse); }
        if (_successCbWithSent) { _successCbWithSent(_nextMsg, _lastResponse); }
        return;
    }

    // No response is considered a problem by this client.
    _isLastConnectionSuccessful = false;
    QString errStr = "Forced Manual Timeout when waiting for response.";
    QAbstractSocket::SocketError errnum = QAbstractSocket::UnknownSocketError;
    if (_errorCallback) { _errorCallback(errStr, errnum); }
    if (_errorCbWithSent) { _errorCbWithSent(_nextMsg, errStr, errnum); }
}

} // namespace evtools
