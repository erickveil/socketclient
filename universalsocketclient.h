/**
 * universalsocketclient.h
 * Erick Veil
 * 2018-09-24
 * Copyright 2018 Erick Veil
 */

#ifndef UNIVERSALSOCKETCLIENT_H
#define UNIVERSALSOCKETCLIENT_H

#include <functional>

#include <QAbstractSocket>
#include <QByteArray>
#include <QDebug>
#include <QHostAddress>
#include <QObject>
#include <QString>
#include <QTcpSocket>
#include <QTimer>
#include <QUuid>

#include "staticlogger.h"

namespace evtools {

/**
 * @brief The UniversalSocketClient class
 * This class allows the simple setup of a socket client.
 * Just build with the appropriate constructor and call sendToSocket to use.
 * Can define optional callbacks for success and failure.
 *
 * Changelog:
 * 2019-07-22 EV	Added catch to prevent sending blank payloads.
 * 2021-03-17 EV	Allow client use to be disabled by a port value <= 0.
 * 2021-10-05 EV	Allow establishing a constant connection with a listener.
 * 					Mostly for testing purposes, as I don't think I'll ever
 * 					use this on purpose in production.
 */
class UniversalSocketClient : public QObject
{
    Q_OBJECT

    QString _socketIp;
    int _socketPort;
    QTcpSocket _socket;
    QByteArray _nextMsg;
    QByteArray _lastResponse;
    bool _isDataInitialised = false;
    bool _isSlotsConnected = false;
    bool _isConstantConnection = false;
    QUuid _uniqueId;

    /**
     * @brief _isLastConnectionSuccessful
     * If we check the _socket error status, it retains the last error it had,
     * even if it is corrected.
     * This value is set false during an error signal, but resets to true
     * whenever we have a completed connection.
     * It can therefore be more reliable for detecting if the socket is
     * currently in an error state.
     */
    bool _isLastConnectionSuccessful = true;

    std::function<void (QByteArray)> _successCallback;
    std::function<void (QByteArray, QByteArray)> _successCbWithSent;
    std::function<void (QString, QAbstractSocket::SocketError)> _errorCallback;
    std::function<void (QByteArray, QString, QAbstractSocket::SocketError)>
        _errorCbWithSent;
    std::function<void ()> _connectionCallback;

    /**
     * @brief _isResponded
     * Flag gets set to true once we receive a response from the listener.
     * When the read timer expires, we check this flag, and if it's true,
     * then we assume we read a response okay.
     * If it's false when we check it, we note that we didn't get a response.
     */
    bool _isResponded = false;

    /**
     * @brief _readTimeoutMs
     * Amount of time to wait for a response after establishing a connection
     * and sending a message.
     *
     * NOTE: The configured poll time cannot be less than or equal to this.
     * That would cause read timeout to trigger after the next poll.
     */
    int _readTimeoutMs = 5000;
    bool _isTimeOutRead = true;

    bool _isExpectingResponse = true;

    QTimer _readTimer;

    int _lastBytesWritten;
    bool _isLastHostFound = false;
    bool _isDoneOnce = false;

public:

    /**
     * @brief NumericAckControl
     * Set to true for Numeric Ack Control
     * The listener will be expected to respond with one or more strings that
     * represents the number of bytes it recieves.
     * Each number is separated by a newline.
     * Each number is compared to the size of the message sent, and if they
     * match, the client will consider the transmission complete and hang up.
     *
     * Default is false - which should keep it to the original behavior.
     *
     * If an ack with the appropriate value is never recieved, then the
     * socket will eventually time out unless it's a constant connection.

     */
    bool NumericAckControl = false;

    // ----- Constructors -----

    /**
     * @brief UniversalSocketClient
     * Will create a null client that will eed manual initialization before
     * it can be used.
     */
    UniversalSocketClient();

    /**
     * @brief UniversalSocketClient
     * Will create a basic socket that just sends data.
     * @param ip
     * @param port
     */
    UniversalSocketClient(QString ip, int port);

    /**
     * @brief UniversalSocketClient
     * Will create a socket client that will fire a callback when it is done.
     * @param ip
     * @param port
     * @param successCallback
     */
    UniversalSocketClient(QString ip, int port,
                          std::function<void (QByteArray)> successCallback);

    /**
     * @brief UniversalSocketClient
     * Will create a complete and ready to use socket client that will fire
     * callbacks on success and failure.
     * @param ip
     * @param port
     * @param successCallback
     * @param errorCallback
     */
    UniversalSocketClient(QString ip, int port,
                          std::function<void (QByteArray)> successCallback,
                          std::function<void (
                              QString,
                              QAbstractSocket::SocketError)> errorCallback);

    /**
     * Destructor
     */
    ~UniversalSocketClient();

    // ----- Initialize Manually -----

    /**
     * @brief setConnectData
     * Allows setting or re-setting of IP and Port for the socket.
     * @param ip
     * @param port
     *
     * Only mandatory to call if the default constructor was used.
     * Is called by all other constructors.
     * Must be called at least once for the object to not be considered null.
     */
    void setConnectData(QString ip, int port);

    /**
     * @brief initSignalsAndSlots
     * Connects the socket signals and slots.
     *
     * This is required before the client will work.
     * This is automatically called from all constructors except the default.
     * Only needs to be called if the default constructor was used.
     * If called for any other constructor, or called more than once, it will
     * return without doing anything (to prevent duplicate slot parsing).
     * Must be called at least once for the object to not be considered null.
     */
    void initSignalsAndSlots();

    /**
     * @brief setSuccessCallback
     * @param successCallback
     *
     * The QByteArray arguement for your callback will be the response from the
     * server, so you can do whatever with it.
     */
    void setSuccessCallback(std::function<void (QByteArray)> successCallback);

    /**
     * @brief setSuccessCallback
     * @param successCallback
     *
     * The first QByteArray is the sent data. The second is the response data.
     */
    void setSuccessCallback(std::function<void (QByteArray, QByteArray)>
                            successCallback);

    /**
     * @brief setErrorCallback
     * @param errorCallback
     */
    void setErrorCallback( std::function<void (
                               QString errStr,
                               QAbstractSocket::SocketError errNo)>
                           errorCallback);

    /**
     * @brief setErrorCallback
     * This error callback also reports the data that was originally sent.
     * @param errorCallback
     */
    void setErrorCallback( std::function<void (
                               QByteArray sent,
                               QString errStr,
                               QAbstractSocket::SocketError errNo)>
                           errorCallback);

    /**
     * @brief setConnectionCallback
     * Callback function for when the socket connects.
     * This is useful for logging when a constant connection is established.
     * @param connectCallback
     */
    void setConnectionCallback(std::function<void ()> connectCallback);

    /**
     * @brief setIsConstantConnection
     * Tells the socket client to remain open.
     *
     * Default behavior is (false) where this client closes the connection as
     * soon as the listener provides a response and readAll returns data.
     *
     * Setting this to true will cause the connection to remain open, and if
     * the remote host closes it, the error callback will be called.
     *
     * @param isConstConnect
     */
    void setIsConstantConnection(bool isConstConnect);

    /**
     * @brief setReadTimeout
     * @param timeMs
     *
     * Optional set the amount of time to wait for a response after sending
     * data into a socket. Default is one second.
     */
    void setReadTimeout(int timeMs);

    /**
     * @brief setIsTimeoutRead
     * @param isTimeoutRead Set to true (default) if you want to time out the
     * read, or false if you want to let TCP take its course.
     * Timing out the read will wait the value set by setReadTimeout (default
     * is 5 seconds) and then call a disconnect on the socket. If set to false,
     * this disconnect will never happen, and only a TCP timeout or completion
     * of the connection with a response will end it.
     */
    void setIsTimeoutRead(bool isTimeoutRead);

    /**
     * @brief setIsExpectingResponse
     * @param isExpectingResponse - set to true (default) if failing to get
     * a response from the listener is considered an error, calling the
     * error Cb.
     * If set to false, we will hang up after sending the data and the read
     * timeout ends and call teh success Cb.
     */
    void setIsExpectingResponse(bool isExpectingResponse);


    // ----- Do Work -----

    /**
     * @brief sendToSocket
     *
     * @param msg: The data to send to the server.
     *
     * The socket runs asynchronously so any responses have to be handled
     * through the success callback.
     */
    void sendToSocket(QByteArray msg);

    /**
     * @brief startConstantConnection
     * Begins a constant connection to the listener without sending any data.
     * NOTE: If a constant connection is interrupted, the connection will
     * become disconnected. If you want a reconnection attempt, it should be
     * handled in the Error Callback.
     */
    void startConstantConnection();

    void sendDataToConstantConnection(QByteArray msg);

    // ----- Validate -----

    /**
     * @brief isClientDisconnected
     * @return true if the client is idle, false if the client is in the process
     * of sending messages.
     *
     * This is good to check before sending if you don't want to spam a server
     * with messages. Maybe the server is touchy, and taking a while to respond.
     * You use this to see if you are already sending something, and then you
     * wait if it is.
     */
    bool isClientDisconnected();

    /**
     * @brief isNull
     * @return true if either the IP or port have not been set, or if the
     * socket signals have not yet been connected.
     * If false, then the socket is ready to use.
     */
    bool isNull();

    /**
     * @brief isDoneOnce
     * @return If the client has completed a connection, wether successful or
     * not, at least one time, this will be true.
     */
    bool isDoneOnce();

    QString getName();
    QString getIp();
    int getPort();

private slots:

    void _sendData();

    // QAbstractSocket
    void _eventSocketConnected();
    void _eventSocketDisconnected();
    void _eventSocketError(QAbstractSocket::SocketError err);
    void _eventSocketHostFound();
    void _eventSocketStateChanged(QAbstractSocket::SocketState state);

    // QIODevice
    void _eventIODeviceAboutToClose();
    void _eventIODeviceBytesWritten(qint64 bytes);
    void _eventIODeviceChannelBytesWritten(int channel, qint64 bytes);
    void _eventIODeviceChannelReadyRead(int channel);
    void _eventIODeviceReadFinished();
    void _eventIODeviceReadyRead();
    void _eventReadTimeout();

};

} // namespace evtools

#endif // UNIVERSALSOCKETCLIENT_H
