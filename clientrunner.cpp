/** Copyright 2021 Erick Veil */

#include "clientrunner.h"

clientRunner::clientRunner()
{

}

void clientRunner::start()
{
    QString ip = "127.0.0.1";
    int port = 50800;

    std::function<void(QByteArray, QByteArray )> doneCb = [&]
            (QByteArray sent, QByteArray response) {
        LOG_INFO("Response from " + sent + " is " + response);
        successCb();
    };


    std::function<void()> connectCb = [&] () {
        LOG_INFO("Connection established");
        timerCb();
    };


    // might need to set flag to not time out?
    client.setConnectData(ip, port);
    client.setConnectionCallback(connectCb);
    client.setSuccessCallback(doneCb);
    client.setIsConstantConnection(true);
    client.setIsExpectingResponse(true);
    client.initSignalsAndSlots();

    client.startConstantConnection();
}

void clientRunner::successCb()
{
    LOG_INFO("Beginning 3 second timer");
    QTimer::singleShot(3000, this, SLOT(timerCb()));
}

QByteArray clientRunner::buildMsg()
{
    QString msg = "Message Number: " + QString::number(msgId);
    ++msgId;
    return msg.toLocal8Bit();
}

void clientRunner::_clientSend(QByteArray msg)
{
    LOG_INFO("Sending: " + msg);

    client.sendDataToConstantConnection(msg);
}

void clientRunner::timerCb()
{
    LOG_INFO("Timer expired");
    QByteArray msg = buildMsg();
    _clientSend(msg);
}
