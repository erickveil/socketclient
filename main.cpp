#include <QCoreApplication>

#include "clientrunner.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    clientRunner client;
    client.start();

    return a.exec();
}
